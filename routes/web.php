<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'ProductController@index')->name('home');
Route::get('/product/{product}', 'ProductController@show')->name('product');

Route::group(['prefix' => 'management'], function () {
    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'ProductController@management')->name('product.management');
        Route::post('/', 'ProductController@create')->name('product.create');
        Route::put('/{product}', 'ProductController@update')->name('product.update');
        Route::delete('/{product}', 'ProductController@delete')->name('product.delete');
    });
    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', 'OrderController@index')->name('order.index');
        Route::post('/', 'OrderController@create')->name('order.create');
        Route::get('/complete/{order}', 'OrderController@complete')->name('order.complete');
        Route::get('/{order}', 'OrderController@show')->name('order.show');
    });
});

Route::group(['prefix' => 'shopping'], function () {
    Route::get('/', 'CartController@showCart')->name('shoppingCart');
    Route::get('/add/{product}', 'CartController@AddToCart')->name('add.shoppingCart');
    Route::get('/remove/{product}', 'CartController@RemoveFromCart')->name('remove.shoppingCart');
    Route::get('/checkout', 'CartController@checkOut')->name('checkOut');

});
Route::group(['prefix' => 'profile'], function () {
    Route::get('/', 'UserController@profile')->name('profile');
    Route::post('/update/{user}', 'UserController@updatePhone')->name('profile.update');
});


Route::post('/sign-up', 'RegisterController@register')->name('register');
Route::get('/sign-up', 'RegisterController@show')->name('register.form');
Route::post('/sign-in', 'LoginController@login')->name('login');
Route::get('/sign-in', 'LoginController@show')->name('login.form');
Route::get('/logout', 'LoginController@logout')->name('logout');
