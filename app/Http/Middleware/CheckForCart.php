<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class CheckForCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Session::has('cart')) {
            return redirect()->route('shoppingCart');
        }

        if (! Session::get('cart')->getTotalQty()) {
            return redirect()->route('shoppingCart');
        }

        return $next($request);
    }
}
