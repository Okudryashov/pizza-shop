<?php

namespace App\Http\Middleware;

use Closure;

class CheckForOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->is_admin) {
            return $next($request);
        }
        
        $str = $request->path();
        $pattern = '/management\/orders\/(.+)/';
        preg_match($pattern, $str, $matches);
        $orders = auth()->user()->orders;
        foreach ($orders as $order) {
            if ($order->id == $matches[1]) {
                return $next($request);
            }
        }

        return redirect()->route('profile')->withErrors('Sorry. You are not allowed there');
    }
}
