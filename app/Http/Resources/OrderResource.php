<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var Order $order
         */

        $order = $this;

        //DONE: user лучше использовать так: UserResource:make($order->user)
        // Зачем user_phone, если при регистрации пользователя лучше просить его обязательно его указать,
        // тогда он будет в объекте пользователя
        return [
            'order' => $order->id,
            'is_completed' => $order->is_completed,
            'user' =>  UserResource::make($order->user),
            'user_phone' => $order->user->phone,
            'products' => new ProductResourceSmallCollection($order->products)
        ];
    }
}
