<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductResourceSmallCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *Use when do not need to include recommendedProducts
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
