<?php


namespace App\Http\Repositories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository
{
    /**
     * @var ImageRepository
     */
    protected $imageRepository;

    /**
     * ProductRepository constructor.
     * @param ImageRepository $imageRepository
     */
    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Product::all();
    }

    /**
     * @param int $id
     * @return Product
     */
    public function show(int $id): Product
    {
        return Product::find($id);
    }

    /**
     * @param string $title
     * @param string $description
     * @param int $price
     * @return Product
     */
    public function store(string $title, string $description, float $price): Product
    {
        $product = new Product();

        $product->title = $title;
        $product->description = $description;
        $product->price = $price;

        $product->save();

        $this->imageRepository->store($product->id);

        return $product;
    }


    /**
     * @param Product $product
     * @throws \Exception
     */
    public function destroy(Product $product): void
    {
        $product->delete();
    }
}
