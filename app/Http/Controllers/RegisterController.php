<?php

namespace App\Http\Controllers;

use App\Http\Repositories\UserRepository;
use App\Http\Requests\RegisterRequest;


class RegisterController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('guest');
    }

    public function show()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->repository->create(
            $request->email,
            $request->password,
            $request->name,
            $request->key
        );

        auth()->login($user);

        session()->flash('message', 'Your registration is succeed');

        return redirect()->route('home');

    }
}
