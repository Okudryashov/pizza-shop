<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{

    public function __construct()
    {
        $this->middleware('is.cart.exist')->only('checkOut');
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addToCart(Product $product)
    {
        $oldCart = $this->getCart();

        $cart = new Cart($oldCart);

        $cart->add($product, $product->id);

        Session::put(['cart' => $cart]);

        return redirect()->back();
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeFromCart(Product $product)
    {
        $oldCart = $this->getCart();

        $cart = new Cart($oldCart);

        $cart->remove($product->id);

        Session::put(['cart' => $cart]);

        return redirect()->back();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCart()
    {

        $oldCart = $this->getCart();

        $cart = new Cart($oldCart);

        return view('shopping-cart', ['products' => $cart->getitems(), 'totalPrice' => $cart->gettotalPrice()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkOut()
    {
        $oldCart = $this->getCart();

        $cart = new Cart($oldCart);

        $bill = $cart->getBill();

        return view('checkOut', ['bill' => $bill]);
    }


    /**
     * @return mixed|null
     */
    public function getCart()
    {
        return Session::has('cart') ? Session::get('cart'): null;
    }

}
