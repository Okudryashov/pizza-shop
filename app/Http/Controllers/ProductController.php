<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCreateRequest;
use App\Models\Product;
use App\Http\Repositories\ProductRepository;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * ProductController constructor.
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('is.auth')->except('index', 'show');
        $this->middleware('is.admin')->except( 'index', 'show');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = $this->repository->all();

        return view('index', ['products' => $products]);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Product $product)
    {
        $product = $this->repository->show($product->id);

        return view('product', ['product' => $product]);
    }

    /**
     * @param ProductCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(ProductCreateRequest $request)
    {
        $this->repository->store(
            $request->title,
            $request->description,
            $request->price
        );

        return redirect()->route('product.management');
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Product $product)
    {
        $this->repository->destroy($product);

        return redirect()->route('product.management');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function management()
    {
        $products = $this->repository->all();

        return view('management', ['products' => $products]);
    }












}
