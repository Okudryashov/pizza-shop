<?php

namespace App\Http\Controllers;

use App\Http\Repositories\OrderRepository;
use App\Http\Requests\OrderCreateRequest;
use App\Models\Order;
use Illuminate\Support\Facades\Session;


class OrderController extends Controller
{
    protected $repository;

    /**
     * OrderController constructor.
     * @param OrderRepository $repository
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('is.auth')->except('create');
        $this->middleware('is.admin')->except('create', 'show');
        $this->middleware('is.allow.order')->only('show');
        $this->middleware('is.cart.exist')->only('create');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $orders = $this->repository->all();

        return view('auth.orders', ['orders' => $orders]);
    }

    /**
     * @param OrderCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(OrderCreateRequest $request)
    {

        $cart = Session::get('cart');

        $this->repository->store(
            $request->phone,
            $request->address,
            $request->details,
            $cart
        );

        $cart = null;

        Session::put('cart', $cart);

        session()->flash('message', 'Your order is created successfully. Please wait for a call from operator');

        if (auth()->user()) {
            return redirect()->route('profile');
        }

        return redirect()->to('/');

    }

    /**
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function complete(Order $order)
    {
        $this->repository->competeOrder($order);

        return redirect()->back();
    }

    /**
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Order $order)
    {
        return view('auth.order', ['order' => $order]);
    }



}
