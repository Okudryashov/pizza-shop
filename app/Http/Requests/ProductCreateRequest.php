<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ProductCreateRequest
 * @package App\Http\Requests
 *
 * @property string $title
 * @property string $description
 * @property string $price
 */
class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:20',
            'description' => 'required|min:10|',
            'price' => 'required:float',
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
