<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class OrderCreateRequest
 * @package App\Http\Requests
 *
 * @property string $phone
 * @property string $address
 * @property string $details
 */
class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required|min:10,max:30',
            'phone' => 'required|regex:/^\+7[0-9]{10}$/'
        ];
    }

    public function messages()
    {
        return  [
            'phone' => 'Please make sure your phone number match with this form: +7',
        ];
    }
}
