<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 *
 * @property int $id
 * @property string $title;
 * @property string $description;
 * @property float $price
 */
class Product extends Model
{

    use SoftDeletes;

    protected $fillable = ['id', 'title', 'description', 'price'];

    public function image()
    {
        return $this->hasOne('App\Models\Image');
    }

    public function order_product()
    {
        return $this->hasOne('App\Models\OrderProduct');
    }






}
