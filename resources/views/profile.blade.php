@extends('layouts.layout')

@section('title', 'Profile')

@section('content')
    @include('layouts.errors')

    <div class="col-md-12">
        <h1>Hello, {{ $user->name }}!</h1>

        <h2>Check your orders</h2>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    №
                </th>
                <th>
                    Phone
                </th>
                <th>
                    Order time
                </th>
                <th>
                    Status
                </th>
                <th>
                    Action
                </th>
            </tr>
            @php $i = 1 @endphp
            @foreach($user->orders as $order)
                <tr>
                    <td> {{ $i }}</td>
                    <td>{{ $order->user_phone }}</td>
                    <td>{{ $order->created_at->format('H:i d/m/Y') }}</td>
                    <td>@if ($order ? $order->is_completed == 1: '') <p class="text-success"> Completed </p> @else <p class="text-danger"> In progress </p> @endif
                    <td><div class="btn-group" role="group">
                            @if ($order ? $order->is_completed == 0 and $user->is_admin == 1: '') <a class="btn btn-success" type="button" href="{{ route('order.complete', ['order' => $order->id])}}">Complete</a> @endif
                            <a class="btn btn-warning" type="button" href="{{ route('order.show', ['order' => $order->id])}}">Open</a>

                        </div>
                    </td>
                </tr>
            @php $i++ @endphp
            @endforeach
            </tbody>
        </table>

        @if($user->phone == false)
        <div class="col-md-4">
        <form method="POST" action="{{ route('profile.update', ['user' => $user->id])}}" aria-label="Login">
            @csrf
            <div class="form-group">
                <label for="phone">Add your phone number and we will use it with orders</label>
                <input id="phone" type="text" class="form-control" name="phone" value="" required autofocus>
            </div>

            <button type="submit" class="btn btn-primary">
              Add phone
            </button>
        </form>
        </div>
            @else
            <h1 class="h4">Your current phone number: <strong> {{ $user->phone }} </strong> </h1>
            @endif
    </div>
@endsection
