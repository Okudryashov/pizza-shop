@extends('layouts.layout')

@section('title', $product->title)

@section('content')
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        <div class="thumbnail  justify-content-between" style="background:rgba(210,207,51,0.56);">
            <img src="{{ $product->image ? $product->image->path: '' }}" alt="Pizza">
            <div class="caption">
                <h3><a href="{{ route('product', ['product' => $product->id])}}"> {{ $product->title }} </a></h3>
                <p class="description"> {{ $product->description }} </p>
                <div class="clearfix">
                    <div class="pull-left price"> {{ $product->price }} $</div>
                    <a href="{{ route('add.shoppingCart', ['product' => $product->id])}}"
                       class="btn btn-success pull-right"
                       role="button">Add to cart</a>
                </div>
            </div>
@endsection
