@extends('layouts.layout')

@section('title', 'Shopping Cart')

@section('content')
    @if(Session::has('cart') and Session::get('cart')->getTotalQty() > 0)
        <div class="container -align-center">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <ul class="list-group">
                    @foreach($products as $product)
                        <li class="list-group-item">
                            <span></span>
                            <div class="btn-group form-inline">
                                <a href="{{ route('remove.shoppingCart', ['product' => $product['item']->id])}}">
                                    <button type="submit" class="btn btn-danger"
                                            href=""><span
                                            class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
                                </a>
                                <a href="{{ route('add.shoppingCart', ['product' => $product['item']->id])}}">
                                    <button type="submit" class="btn btn-success"
                                            href=""><span
                                            class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </a>
                            </div>
                            <span class="badge">{{ $product['qty'] }}</span>
                            <strong>{{ $product['item']['title'] }}</strong>
                            <span class="label label-success"> {{ $product['price'] }} $</span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <strong> Total: {{ $totalPrice }} $ </strong>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <a href="{{ route('checkOut') }}" class="btn btn-success"> Checkout </a>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-sm-6 col-md-6 ">
                <h1 class="h4"> Your cart is empty now. <a href="/"> Come back </a> and choose something tasty </h1>
            </div>
        </div>
        </div>
    @endif

@endsection
