@extends('layouts.layout')

@section('title', 'Sign up')

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1 class="text-center  mb-3 font-weight-normal">Sign Up</h1>
            @include('layouts.errors')
            <form method="POST" action="{{route('register')}}" aria-label="Login">
                @csrf
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input id="email" type="email" class="form-control" name="email" value="" required autofocus>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" type="password" class="form-control" name="password" value="" required autofocus>
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label>
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" value="" required autofocus>
                </div>

                <div class="form-group">
                    <label for="name">Your name</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="" required autofocus >
                </div>

                <div class="form-group">
                    <label for="secret-key">Know a secret key?</label>
                    <input type="password" name="key" id="secret-key" class="form-control" placeholder="">
                </div>

                <button type="submit" class="btn btn-primary">
                    Sign Up
                </button>
            </form>
        </div>
    </div>


@endsection

