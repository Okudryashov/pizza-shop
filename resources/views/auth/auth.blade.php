@extends('layouts.layout')

@section('title', 'Sign in')

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
        <h1 class="text-center  mb-3 font-weight-normal ">Sign In</h1>
            @include('layouts.errors')
            <form method="POST" action="{{ route('login') }}" aria-label="Login">
            @csrf
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input id="email" type="email" class="form-control" name="email" value="" required autofocus>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" type="password" class="form-control" name="password" value="" required autofocus>
                </div>

                <button type="submit" class="btn btn-primary">
                    Sign In
                </button>

                <p class="text-info">
                    Do not have an account? <a href="{{ route('register.form') }}"> Sign up! </a>
                </p>
            </form>
        </div>
    </div>


@endsection
