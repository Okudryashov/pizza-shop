@extends('layouts.layout')

@section('title', 'Order ' . $order->id)

@section('content')
    <div class="py-4">
        <div class="container">
            <div class="justify-content-center">
                <div class="panel">
                    <h1>Order №{{ $order->id }}</h1>
                    <p>Customer: <b>@if($order->user) {{ $order->user->name }} @endif </b></p>
                    <p>Phone number: <b>{{ $order->user_phone }}</b></p>
                    <p> Order time: <b>{{ $order->created_at->format('H:i d/m/Y') }}</b></p>
                    <p> Delivery address: <b>{{ $order->address }}</b></p>
                    <p> Order details: <b>{{ $order->details }}</b></p>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Quantity</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        @foreach ($order->products as $product)
                            <tr>
                                <td>

                                        <img height="56px"
                                             src="{{ $product->image->path }}">
                                        {{ $product->title }}
                                </td>
                                <td><span class="badge"> {{$product->pivot->quantity}}  </span></td>
                                <td>{{ $product->price }} $</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3"><strong> Total cost: {{ $order->calculatefullsum() }}$ </strong> </td>
                        </tr>
                    </table>
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection
