@extends('layouts.layout')

@section('title', 'Management')

@section('content')
    <div class="col-md-12">
        <h1>Goods</h1>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    id
                </th>
                <th>
                    Title
                </th>
                <th>
                    Description
                </th>
                <th>
                    Price
                </th>
                <th>
                    Actions
                </th>
            </tr>
            @foreach($products as $product)
                <tr>
                    <td><strong> {{ $product->id}} </strong></td>
                    <td class="text-success"><p class="text-success>"> {{ $product->title }} </p> </td>
                    <td>{{ $product->description }}</td>
                    <td>{{ $product->price }} $</td>
                    <td>
                        <div class="btn-group" role="group">
                            <form action="{{ route('product.delete', ['product' => $product->id]) }}" method="POST">
                                <a class="btn btn-success" type="button"
                                   href="{{ route('product', ['product' => $product->id]) }}">Open</a>
                                @csrf
                                {{method_field('DELETE')}}
                                <input class="btn btn-danger" type="submit" value="Delete"></form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="col-md-8 col-md-offset-2">

        @include('layouts.errors')

        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
        <form method="post" action="{{ route('product.create') }}" enctype="multipart/form-data" class="px-10 py-10">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="What is the title of our new pizza?">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" name="description" id="description" placeholder="Let's tell something about our new pizza">
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                <input type="text" class="form-control" name="price" id="price" placeholder="What is the cost?">
            </div>

            <div class="form-group" >
                <label class="file" for="file">State the link to the image</label>
                <input type="text" name="imagePath"  class="form-control"  id="file">
            </div>

            <div class="form-group">
            <button type="submit" class="btn btn-success">Add a new pizza!</button>
            </div>
        </form>
    </div>
    </div>

@endsection
